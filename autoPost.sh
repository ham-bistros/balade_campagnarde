#!/bin/bash

PATH=$(cd `dirname $0` && pwd)
cd $PATH
for f in $PATH/*.py; 
 do /usr/bin/python3 $f >> "${f%.*}.log" 2>&1;
done
