#!/usr/bin/env python3
#coding: utf-8

###BASE
import json
import folium

###CHANGEMENT DE COULEURS
import matplotlib.colors as mpl_col
import numpy as np
###fonction chgmt couleurs
def colorFader(c1,c2,mix=0): #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1=np.array(mpl_col.to_rgb(c1))
    c2=np.array(mpl_col.to_rgb(c2))
    return mpl_col.to_hex((1-mix)*c1 + mix*c2)

########################## LISTE COMMUNES #######################
with open('balade_dépendences/liste_communes_clean.json', 'r', encoding="utf8") as f:
    data = f.read()
    d = json.loads(data)

########################## COMMUNES DÉJÀ VISITÉES #######################
with open('communes_visitées', 'r', encoding='utf-8') as f:
    data = f.read()
    communes = data.splitlines()

liste_points = []
###créer une liste de points avec les coordonnées des communes visitées
for commune in communes:
    index = d['nom_commune'].index(commune)
    coordonnées = tuple([float(d['latitude'][index]), float(d['longitude'][index])])
    liste_points.append(coordonnées)

########################## CRÉER LA CARTE #######################
####initialize the map
franceMap = folium.Map(location=[46.9, 2], tiles='Stamen Toner', zoom_start=6)

###!!!ne fonctionne que si ce sont des nombres dans les tuples des coordonnées et non des strings
for i, commune in enumerate(communes):
    color = colorFader('blue', 'red', i/len(communes))
    if i != 0:
        ###créer des lignes pour chaque point (chaque village parcouru)
        folium.PolyLine([liste_points[i-1], liste_points[i]], color=color, weight=2, opacity=1).add_to(franceMap)

for i, commune in enumerate(communes):
    color = colorFader('blue', 'red', i/len(communes))
    if i != 0:
        ###créer des points pour afficher le nom de chaque commune
        folium.CircleMarker((liste_points[i][0], liste_points[i][1]), radius=2, weight=1, color=color, fill_color=color, fill_opacity=1, tooltip=commune).add_to(franceMap)

###départ
folium.CircleMarker((liste_points[0][0], liste_points[0][1]), radius=4, weight=3, color='black', fill_color='blue', fill_opacity=1, tooltip="Griscourt (DÉPART)").add_to(franceMap)
###arrivée
folium.CircleMarker((liste_points[len(liste_points)-1][0], liste_points[len(liste_points)-1][1]), radius=4, weight=3, color='black', fill_color='red', fill_opacity=1, tooltip=commune+' (ARRIVÉE)').add_to(franceMap)

###save the map as an html file
franceMap.save('FrancePointMap.html')
print('C\'est la carte')
