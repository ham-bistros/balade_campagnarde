#!/usr/bin/env python3
#coding: utf-8

###############################SCRAPING WIKIPEDIA####################
import lxml.html as parser
import cssselect, mechanize, re
import urllib.parse as urlP

###avec mechanize
br = mechanize.Browser()
# br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0')]
br.addheaders = [('User-agent', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36')]
br.set_handle_robots(False)

commune_selec = 'Ploëzec'.lower()
commune_selec = commune_selec.encode('utf-8')

url = 'https://www.google.com/search?hl=en&tbm=isch&q=' + urlP.quote(commune_selec) + '&source=lnms'

print(url)
data = br.open(url, timeout=50)
source = data.read()

###regex pour choper l'URL de l'image dans le code source, parce qu'il est pas dans une balise donc on peut pas le trouver avec css_select
img = re.findall(r'\["([^"]+\.jpg)",[0-9]+,[0-9]+\]', str(source))

###téléchargement de la première image
###si la première ne marche pas on essaie la suivante jusqu'à ce que ça marche
n = 0
while True:
    try:
        br.retrieve(img[n], 'balade_dépendences/image.jpg')
        break
    except:
        n += 1
