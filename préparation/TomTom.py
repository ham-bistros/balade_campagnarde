#!/usr/bin/env python3
#coding: utf-8

import json
import datetime
from urllib.request import urlopen

api_key = '******************************'

with open('../balade_dépendences/result.json', 'r', encoding="utf8") as f:
    data = f.read()
    d = json.loads(data)

départ = 'Tressignaux'
index_dernière_commune = d['nom_commune'].index(départ)
lat_base = d['latitude'][index_dernière_commune]
long_base = d['longitude'][index_dernière_commune]

commune_selec = 'Binic'
index_commune_selec = d['nom_commune'].index(commune_selec)
lat_fin = d['latitude'][index_commune_selec]
long_fin = d['longitude'][index_commune_selec]

tomtom_url = 'https://api.tomtom.com/routing/1/calculateRoute/{0}%2C{1}%3A{2}%2C{3}/json?avoid=unpavedRoads&travelMode=pedestrian&key={4}'.format(lat_base, long_base, lat_fin, long_fin, api_key)

getData = urlopen(tomtom_url).read()
result = json.loads(getData)

for i in result['routes'][0]['summary']:
    print(i, result['routes'][0]['summary'][i])
# with open('json_test.json', 'a', encoding='utf8') as f:
#     json.dump(result, f)
