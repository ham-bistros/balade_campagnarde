#!/usr/bin/env python3
#coding: utf-8


from accès import *
import tweepy


auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

#api.update_status(status="coucou les amis")

#Cursor() permet de naviguer dans le contenu de Twitter
#.items limite le nombre de requêtes à 10
#q est le mot que l'on recherche
#extended permet d'avoir les 280 caractères des tweets

tweets = tweepy.Cursor(api.search, q='patate', tweet_mode='extended').items(10)

for tweet in tweets:
    #tweet contient le texte mais aussi plein de métadonnées
    #pour avoir juste le texte, on print tweet.text
    print('###'+tweet.full_text+'###')

users = ['@camcamcamembert', '@poete_nrv', ]
#
# for user in users:
#     personne = api.get_user(user)
#     #print (user.id)
#     api.send_direct_message(personne.id, 'coucou')
