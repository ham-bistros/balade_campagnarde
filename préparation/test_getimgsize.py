#!/usr/bin/env python3
#coding: utf-8

import urllib
from PIL import ImageFile

def getsizes(uri):
    # get file size *and* image size (None if not known)
    file = urllib.request.urlopen(uri)
    size = file.headers.get("content-length")
    if size:
        size = int(size)
    p = ImageFile.Parser()
    while True:
        data = file.read(1024)
        if not data:
            break
        p.feed(data)
        if p.image:
            return size, p.image.size
            break
    file.close()
    return(size, None)

size = getsizes('https://www.google.com/search?q=tressignaux&sxsrf=ALeKk01C2V0GdlUqWcFpO5jnov1TFDug4Q:1587837174329&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjtovK1koTpAhVQsaQKHf9WC04Q_AUoA3oECBcQBQ&biw=1366&bih=613#imgrc=E9yb7bHCsyaL1M')
print(size)
