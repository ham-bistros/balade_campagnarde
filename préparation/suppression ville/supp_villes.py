#!/usr/bin/env python3
#coding: utf-8

import lxml.html as parser
import cssselect, mechanize, re
import urllib.parse as urlP

###avec mechanize
br = mechanize.Browser()
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0')]
br.set_handle_robots(False)

###liens cassés
# wiki_base = ''
###normalement c'est la page wikipédia de la liste des communes de france (liste des départements)
# url_base = ''

def getLiens(url, css_select, type, dpt):
    data = br.open(url, timeout=50)
    source = data.read()

    tree = parser.fromstring(source)

    liens = []
    habitants = []
    commune = []

    ###dans cette liste on met les index des communes qui ont plus de 1999 habitants
    sup = []
    ###index des communes de + de 1500 habitants
    n = 0

    if type == 'liens':
        for selector in cssselect.parse(css_select):
            ##on convertit l'objet selector en xpath
            xpath_selector = cssselect.HTMLTranslator().selector_to_xpath(selector)
            ##pour chaque lien trouvé par ce xpath
            for link in tree.xpath(xpath_selector):
                href = link.get('href')
                liens.append(wiki_base+href)
        return liens

    elif type == 'habitants':
        if dpt == 44:
            css_select = 'table.wikitable.sortable.titre-en-couleur td:nth-child(8)'
        elif dpt == 48:
            css_select = 'table.wikitable.sortable:nth-of-type(2) td:nth-child(8)'
        elif dpt == 70:
            css_select = 'table.wikitable.sortable.titre-en-couleur td:nth-child(5)'
        elif dpt == 95:
            css_select = 'table.wikitable.sortable.titre-en-couleur td:nth-child(8)'
        elif dpt == 103:
                css_select = 'table.wikitable.sortable.titre-en-couleur td:nth-child(5)'


        for selector in cssselect.parse(css_select):
            ##on convertit l'objet selector en xpath
            xpath_selector = cssselect.HTMLTranslator().selector_to_xpath(selector)
            ##pour chaque lien trouvé par ce xpath
            for link in tree.xpath(xpath_selector):
                hab = link.text
            ###debugging du bug de liens
                try:
                    hab = int(re.sub(r"\s+", "", hab, flags=re.UNICODE))
                except:
                ###si la commune est buggée ou qu'il n'y a pas l'info du nombre d'habitants, on met une valeur très grande pour être sûr que la commune soit supprimée dans le csv
                    hab = 99999

                # print(hab)
                habitants.append(hab)
                if hab > 1999:
                    sup.append(n)
                n +=1

        if dpt == 56:
            css_select_2 = 'table.wikitable.sortable td:nth-child(1)'
        else:
            css_select_2 = 'table.wikitable.sortable.titre-en-couleur td:nth-child(1)'

        # for selector in cssselect.parse('table.wikitable.sortable:nth-of-type(1) td:nth-child(1)'):
        for selector in cssselect.parse(css_select_2):
            ##on convertit l'objet selector en xpath
            xpath_selector = cssselect.HTMLTranslator().selector_to_xpath(selector)
            ###pour chaque lien trouvé par ce xpath
            for link in tree.xpath(xpath_selector):
                com = link.text_content().rstrip()
                ###suppriemer les parenthèses si besoin
                com = com.rstrip('*')
                com = re.sub(r"\(.+\)", "", com)
                commune.append(com)
        dpt += 1
        return habitants, commune, sup

    else:
        print('mauvais type')

liens_dpt = getLiens(url_base, 'td:nth-child(5) a', 'liens', 0)

count = 0
#########################DEBUGGING##########################

###afficher les liens obtenus sur la première page
for i in liens_dpt:
    # print(i)
    # ###faire l'opération pour tous les départements fr
    # print(getLiens(i, 'table.wikitable.sortable:nth-of-type(1) td:nth-child(8)', 'habitants', count)[1])

### choper les noms des communes et le nombre d'habitants de chaque département
    try:
        alors = getLiens(i, 'table.wikitable.sortable:nth-of-type(1) td:nth-child(8)', 'habitants', count)
        # with open('villes1999', 'a', encoding='utf8') as f:
        #     for j in alors[2]:
        #         f.write(alors[1][j] + '\n')
        #         print(alors[1][j])

###problème de longueur des arrays : il prend en compte les mauvais tableaux pour :nth-child(1)
        # if len(alors[0]) == len(alors[1]):
        #     print('yes')
        # else:
        #     print('%d oh non' % count)
    except:
        print('%d bug de liens' % count)

    count += 1

num = 103
alors = getLiens(liens_dpt[num], 'table.wikitable.sortable:nth-of-type(1) td:nth-child(8)', 'habitants', num)

###checker la longueur de l'array
# for i in alors[2]:
#     # print(i)
#     print('%s, %s' % (alors[1][i], alors[0][i]))
# print('len_arr =  %s' % len(alors[2]))


# print(len(alors[0]))
# print(len(alors[2]))

###checker si la commune a le bon nombre d'habitants
# for i in range(len(alors[1])):
#     # print(alors[1][i])
#     print('%s, %s habitants' % (alors[1][i], alors[0][i]))
# print('com=%s,   hab= %s' % (len(alors[1]), len(alors[0])))
