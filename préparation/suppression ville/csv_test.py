#!/usr/bin/env python3
#coding: utf-8

import csv, json

###fonction pour supprimer les lignes qu'on veut pas
def supp(arr):
    for didi in d:
        for r in range(len(arr)):
            ###puisque pop() enlève une valeur il change l'index de toutes les autres. Il faut donc compenser en faisant -r, pour retrouver la bonne valeur
            d[didi].pop(arr[r]-r)

###on crée le dictionnaire pour contenir les valeurs du tableau. Chaque colonne devient une liste rangée dans un dictionnaire d
d = {}
d['index_commune'] = []
d['nom_commune'] = []
d['latitude'] = []
d['longitude'] = []

rows = []

##########################LIRE LE CSV##########################
with open('../tableau_simplifié.csv', 'r+') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        for i in range (0,4):
            valeur = list(row.items())[i][0]
            d[valeur].append(row[valeur])

##########################ENLEVER LES LIGNES VIDES (SANS LONG/LAT)###########################
for i in range(len(d['longitude'])):
    if d['longitude'][i] == "" or d['longitude'][i] == "-":
        # print('vide')
        rows.append(i)

###il y a 2875 communes pour lesquelles on a pas la valeur de la latitude/longitude : on les supprime du csv
supp(rows)

##########################ENLEVER LES COMMUNES DE + DE 2000 HAB###########################
with open('villes1999', 'r+', encoding='utf8') as g:
    file = g.read()
    villes = file.splitlines()

liste_villes = []

for r in range(len(d['nom_commune'])):
    # print(d['nom_commune'][r])
    if d['nom_commune'][r] in villes:
        liste_villes.append(r)
###on supprime aussi les 5413 villes
supp(liste_villes)

##########################ENLEVER LES COMMUNES EN DOUBLE###########################
###trouvé sur stackoverflow, crée une array de noms de communes en double dans 'dupes'
seen = {}
dupes = []
for x in d['nom_commune']:
    if x not in seen:
        seen[x] = 1
    else:
        if seen[x] == 1:
            dupes.append(x)
        seen[x] += 1

###crée une liste d'indexs à supprimer dans le dictionnaire d
a_sup = []
for doublon in dupes:
    ###ajoute le premier index trouvé à la liste a_sup
    a_sup.append(d['nom_commune'].index(doublon))

###on supprime tout ça
supp(a_sup)

##########################ON ÉCRIT LE TOUT DANS LE FICHIER JSON###########################
with open('liste_communes_clean.json', 'w', encoding="utf8") as fp:
    json.dump(d, fp)
