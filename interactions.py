#!/usr/bin/env python3
#coding: utf-8

from time import sleep
import random
import tweepy
from access import *

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
api = tweepy.API(auth, wait_on_rate_limit=True)

# with open('communes_visitées', 'r', encoding='utf-8') as f:
#     data = f.read()
#     communes = data.splitlines()

phrases = ['Je suis déjà passé par %s !', 'Je suis déjà allé à %s :)', 'Je la trouve sympa la commune de %s.', 'Tiens, %s me dit quelque chose.']

# for commune in communes:
#     sleep(random.randint(20, 40))
#     try:
#         for status in tweepy.Cursor(api.search, q=commune, lang='fr', tweet_mode='extended').items(1):
#             tweet_id = status.id
#             tweet_username = status.user.screen_name
#
#             ###pour ne pas se répondre à soi-même
#             if tweet_username == 'baladecampagne':
#                 print('on ne se répond pas à soi enfin')
#                 break
#
#             ###si le tweet est retweeté, afficher le texte du retweet
#             if hasattr(status, 'retweeted_status'):
#                 tweetText = status.retweeted_status.full_text
#             else:
#                 tweetText = status.full_text
#         if commune.lower() in tweetText.lower():
#             tweet = tweetText
#             message = random.choice(phrases)
#             print(commune.upper(), tweet)
#             print('@{0} {1}'.format(tweet_username, message) % commune, tweet_id)
#             api.update_status('@{0} {1}'.format(tweet_username, message) % commune, tweet_id)
#         else:
#             print('Pas de tweet trouvé avec ce nom de commune, on continue')
#     except NameError:
#         print('Pas de tweet trouvé avec ce nom de commune, on continue')


# with open('commune.txt', 'w', encoding='utf-8') as f:
#     i = 0
#     for status in tweepy.Cursor(api.user_timeline).items(5):
#         if not status.entities['user_mentions']:
#             print(i+1, str(status.text)+'\n')
#             f.write(str(status)+'\n')
#             i += 1
